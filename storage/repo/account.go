package repo

import "time"

type AccountStorageI interface {
	Create(a *Account) (*Account, error)
	Get(accountId int64) (*Account, error)
	Update(accountId int64, balance float64) (*Account, error)
	Delete(accountId int64) error
	GetAll(params *AccountParams) (*Accounts, error)
}

type Account struct {
	Id        int64
	Owner     string
	Balance   float64
	Currency  string
	CreatedAt time.Time
}

type Accounts struct {
	Accounts []*Account
	Count    int64
}

type AccountParams struct {
	Limit  int64
	Page   int64
	Owner  string
	SortBy string
}
