package postgres_test

import (
	"database/sql"
	"testing"
	"time"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/SaidovZohid/simple_bank/pkg/utils"
	"gitlab.com/SaidovZohid/simple_bank/storage/repo"
)

func createAccount(t *testing.T) *repo.Account {
	model := &repo.Account{
		Owner:    faker.FirstName(),
		Balance:  utils.RandomMoney(),
		Currency: utils.RandomCurrency(),
	}
	account, err := dbManager.Account().Create(model)
	require.NoError(t, err)
	require.NotEmpty(t, account)

	require.Equal(t, model.Owner, account.Owner)
	require.Equal(t, model.Balance, account.Balance)
	require.Equal(t, model.Currency, account.Currency)

	require.NotZero(t, account.Id)
	require.NotZero(t, account.CreatedAt)
	return account
}

func TestCreateAccount(t *testing.T) {
	account := createAccount(t)
	err := dbManager.Account().Delete(account.Id)
	require.NoError(t, err)
}

func TestGetAccount(t *testing.T) {
	account1 := createAccount(t)
	account2, err := dbManager.Account().Get(account1.Id)
	require.NoError(t, err)
	require.NotEmpty(t, account2)

	require.Equal(t, account1.Id, account2.Id)
	require.Equal(t, account1.Owner, account2.Owner)
	require.Equal(t, account1.Balance, account2.Balance)
	require.Equal(t, account1.Currency, account2.Currency)
	require.WithinDuration(t, account1.CreatedAt, account2.CreatedAt, time.Second)
	err = dbManager.Account().Delete(account1.Id)
	require.NoError(t, err)
}

func TestUpdateAccount(t *testing.T) {
	account1 := createAccount(t)

	arg := &repo.Account{
		Id:      account1.Id,
		Balance: utils.RandomMoney(),
	}

	account2, err := dbManager.Account().Update(arg.Id, arg.Balance)
	require.NoError(t, err)
	require.NotEmpty(t, account2)

	require.Equal(t, account1.Id, account2.Id)
	require.Equal(t, account1.Owner, account2.Owner)
	require.Equal(t, account1.Balance, account2.Balance)
	require.Equal(t, account1.Currency, account2.Currency)
	require.WithinDuration(t, account1.CreatedAt, account2.CreatedAt, time.Second)
	err = dbManager.Account().Delete(account1.Id)
	require.NoError(t, err)
}

func TestDeleteAccount(t *testing.T) {
	account1 := createAccount(t)
	err := dbManager.Account().Delete(account1.Id)
	require.NoError(t, err)

	account2, err := dbManager.Account().Get(account1.Id)
	require.NoError(t, err)
	require.EqualError(t, err, sql.ErrNoRows.Error())
	require.Empty(t, account2)
}

func TestGetAllAccounts(t *testing.T) {
	for i := 0; i < 10; i++ {
		createAccount(t)
	}

	arg := &repo.AccountParams{
		Limit: 10,
		Page:  1,
	}

	accounts, err := dbManager.Account().GetAll(arg)
	require.NoError(t, err)
	// ! here is an error
	require.Len(t, *accounts, 5)
	require.Equal(t, accounts.Count, 10)
	for _, account := range accounts.Accounts {
		require.NotEmpty(t, account)
		err = dbManager.Account().Delete(account.Id)
		require.NoError(t, err)
	}
}
